# Curso de Redis - Filipe Morelli

O Redis é um repositório de valor-chave avançado com licença BSD e código-fonte aberto. É geralmente chamado de servidor de estrutura de dados, pois as chaves podem conter strings, hashes, listas, conjuntos e conjuntos de classificação.
O Redis é escrito em C. Com Redis é simples de implementar um sistema altamente escalável e orientado ao desempenho.
